from setuptools import setup, find_packages

setup(
    name='gun',
    description='',
    version='1.0',
    packages=find_packages(),
    install_requires=[
        'numpy==1.11.1',
        'matplotlib==1.5.3'
    ],
    entry_points='''
        [console_scripts]
        gun=gun.gun:gun
    '''
)
