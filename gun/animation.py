"""
A simple example of an animated plot
Inspired by: http://matplotlib.org/examples/animation/simple_anim.html
"""

import time

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class Animation:

    def __init__(self, x: np.ndarray, y: np.ndarray, T: float):
        self.fig, self.ax = plt.subplots()
        self.x = x
        self.y = y
        self.T = T
        self.t0 = time.time()
        self.line, = self.ax.plot(self.x, self.y)
        self.ax.grid('on')
        self.ax.axis('equal')

    def _animate(self, _i):
        t = time.time() - self.t0
        n = int(len(self.x) * t / self.T)
        self.line.set_xdata(self.x[:n])
        self.line.set_ydata(self.y[:n])  # update the data
        return self.line,

    # Init only required for blitting to give a clean slate.
    def _init(self):
        self.line.set_ydata(np.ma.array(self.x, mask=True))
        return self.line,

    def run(self):
        anim = animation.FuncAnimation(self.fig, self._animate, init_func=self._init,
                                       interval=10, blit=True)
        plt.show()


if __name__ == '__main__':
    x = np.arange(0, 2 * np.pi, 0.01)
    y = np.sin(x)
    Animation(x, y, 5).run()
