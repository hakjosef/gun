from gun.animation import Animation
from gun.equations import throw


def fun():
    v0 = 10      # m/s
    alpha = 20   # °

    x, y, t, T, D, H = throw(v0, alpha)
    Animation(x, y, T).run()

if __name__ == '__main__':
    fun()
