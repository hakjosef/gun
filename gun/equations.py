import numpy as np


def throw(v0, alpha):
    alpha = np.pi * alpha / 180   # Deg to rad.
    g = 10
    D = v0**2 * np.sin(2 * alpha)
    T = v0 * np.sin(2 * alpha) / (g * np.cos(alpha))
    H = v0 ** 2 * np.sin(2 * alpha) ** 2 / (2 * g)
    t = np.arange(0, T, 0.01)
    x = v0 * t * np.cos(alpha)
    y = v0 * t * np.sin(alpha) - 0.5 * g * t ** 2
    return x, y, t, T, D, H
